const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp(functions.config().firebase);

const express = require("express");
const cors = require("cors");
const app = express();

const db = admin.firestore();

app.use(cors());

// const auth = (req, res, next) => {
//     if (req.body.apiKey || req.headers['x-api-key']) {
//         db.collection('apiKeys').doc(req.body.apiKey || req.headers['x-api-key']).get().then(apiKey => {
//             if (apiKey.data() && apiKey.data().isActive) {
//                 next();
//             } else {
//                 res.status(400).send({
//                     status: false,
//                     error: 'auth not found'
//                 });
//             }
//         }).catch(err => {
//             res.status(400).send({
//                 status: false,
//                 error: 'auth notfound'
//             });
//         });

//     } else {
//         res.status(400).send({
//             status: false,
//             error: "not have api key"
//         });
//     }
// };

app.post("/addUser", (req, res) => {
    const user = req.body
    if (req.body.uid) {

        user.id = user.uid
        user.createAt = admin.firestore.FieldValue.serverTimestamp()
        user.type = "user"

        delete user.password
        delete user.confirmPassword
        delete user.uid

        db.collection('users')
            .doc(user.id)
            .set(user)
            .then(resAdduser => {
                res.status(200).send({
                    status: true,
                    data: user
                });
            })
            .catch(err => {
                res.status(400).send({
                    status: false,
                    data: err.message
                });
            });
    } else {
        res.status(400).send({
            status: false,
            data: 'not req'
        });
    }
});

exports.userAPI = functions.https.onRequest(app)


// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });

const functions = require("firebase-functions");
const admin = require("firebase-admin");

const express = require("express");
const cors = require("cors");
const app = express();

const db = admin.firestore();

app.use(cors());

app.post("/addDataAutoId", (req, res) => {
    const body = req.body;
    if (body.uid && body.collection) {
        body.isActive = true;
        body.isDeleted = false;
        body.createdAt = admin.firestore.FieldValue.serverTimestamp();
        body.createdBy = body.uid;
        delete body.uid;
        db.collection(body.collection).add(body)
            .then(resAdd => {
                db.collection(body.collection).doc(resAdd.id).update({
                    id: resAdd.id
                }).then(success => {
                    res.status(200).send({
                        status: true,
                        data: resAdd.id
                    });
                }).catch(err => {
                    res.status(400).send({
                        status: false,
                        data: err.message
                    });
                });

            })
            .catch(err => {
                res.status(400).send({
                    status: false,
                    data: err.message
                });
            });
    } else {
        res.status(400).send({
            status: false,
            data: 'not req'
        });
    }

});

function addHistory(id, uid, collection, data) {
    if (id && uid && collection && data) {
        data.createdAt = admin.firestore.FieldValue.serverTimestamp();
        data.createdBy = uid;
        db.collection(collection).doc(id).collection('history').add(data).then(resAdd => {
            db.collection(collection).doc(id).collection('history').doc(resAdd.id).update({
                id: resAdd.id
            }).catch(err => {
                console.error(err);
            });
        }).catch(err => {
            console.error(err);
        });
    }
}

app.post("/updateData", (req, res) => {
    const body = req.body;
    if (body.id && body.uid && body.collection) {
        const data = Object.assign({}, body);
        data.updatedBy = body.uid;
        data.updatedAt = admin.firestore.FieldValue.serverTimestamp();
        delete data.id;
        delete data.uid;
        delete data.collection;
        db.collection(body.collection).doc(body.id).update(data).then(success => {
            res.status(200).send({
                status: true,
                data: body.id
            });
            addHistory(body.id, body.uid, body.collection, data);
        }).catch(err => {
            res.status(400).send({
                status: false,
                data: err.message
            });
        });


    } else {
        res.status(400).send({
            status: false,
            data: 'not req'
        });
    }
});
app.post("/deleteData", (req, res) => {
    const body = req.body;
    if (body.id && body.uid && body.collection) {
        const dataUpdate = {
            isActive: false,
            isDeleted: true,
            updatedBy: body.uid,
            updatedAt: admin.firestore.FieldValue.serverTimestamp()
        };
        db.collection(body.collection).doc(body.id).update(dataUpdate).then(success => {
            res.status(200).send({
                status: true,
                data: body.id
            });
            addHistory(body.id, body.uid, body.collection, dataUpdate);
        }).catch(err => {
            res.status(400).send({
                status: false,
                data: err.message
            });
        });


    } else {
        res.status(400).send({
            status: false,
            data: 'not req'
        });
    }
});

exports.productAPI = functions.https.onRequest(app)
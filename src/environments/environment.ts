// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCDzENMD3r-rVomLprxY6gvZ9NP39MJ6yc",
    authDomain: "practical-ecommerce-be9c7.firebaseapp.com",
    projectId: "practical-ecommerce-be9c7",
    storageBucket: "practical-ecommerce-be9c7.appspot.com",
    messagingSenderId: "633105518074",
    appId: "1:633105518074:web:96fc11d419a6d86ce56843",
    measurementId: "G-H9EP2P7ZV3"
  },
  apiURL: "https://us-central1-practical-ecommerce-be9c7.cloudfunctions.net/apiV1",
  productsAPI: "https://us-central1-practical-ecommerce-be9c7.cloudfunctions.net/apiV2",
  apiKey: "nPLO6daT9riuk0Hi42us" // api for dev
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

import { Component, OnInit, ElementRef } from '@angular/core';
import { ROUTES } from '../sidebar/sidebar.component';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  authState: any = null
  fetching: { [key: string]: any } = {}
  public user: any;
  public focus;
  public listTitles: any[];
  public location: Location;
  constructor(location: Location, private element: ElementRef, private router: Router, public authService: AuthService) {
    this.location = location;
    firebase.auth().onAuthStateChanged(res => {
      this.authState = res;
      if (this.authState) {
        if (this.fetching['getUser']) {
          this.fetching['getUser']()
        } else {
          this.fetching['getUser'] = firebase.firestore().collection('users').doc(this.authState.uid).onSnapshot(User => {
            if (User.data()) {
              this.user = User.data()
              console.log(this.user)
            }
          });
        }
      } else {
        this.toLogin()
      }

    }, err => {
      console.error(err)
    })
  }

  ngOnInit() {
    this.listTitles = ROUTES.filter(listTitle => listTitle);
  }

  getTitle() {
    var titlee = this.location.prepareExternalUrl(this.location.path());
    if (titlee.charAt(0) === '#') {
      titlee = titlee.slice(1);
    }

    for (var item = 0; item < this.listTitles.length; item++) {
      if (this.listTitles[item].path === titlee) {
        return this.listTitles[item].title;
      }
    }
    return 'Dashboard';
  }

  toLogin() {
    this.router.navigate(['login'])
  }

}

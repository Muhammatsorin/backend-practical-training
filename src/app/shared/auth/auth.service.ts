import { Router } from '@angular/router'
import { Injectable } from '@angular/core'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import { ApiService } from '../apis/api.service'

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  token: string
  authState: any = null
  autoSignIn = true
  currentAuthState: any
  fetching: { [key: string]: any } = {}
  public loading: boolean
  public user: any

  constructor(public router: Router, private apiService: ApiService) {
    firebase.auth().onAuthStateChanged(res => {
      this.authState = res;
      if (this.authState) {
        if (this.fetching['getUser']) {
          this.fetching['getUser']()
        } else {
          this.fetching['getUser'] = firebase.firestore().collection('users').doc(this.authState.uid).onSnapshot(User => {
            if (User.data()) {
              this.user = User.data()
              // console.log(this.user) 
              if (this.user.type === 'admin') {
                this.router.navigate(['dashboard'])
              } else {
                this.toLogin()
              }
            }
          });
        }
      } else {
        this.toLogin()
      }

    }, err => {
      console.error(err)
    })
  }

  toLogin() {
    this.router.navigate(['login'])
  }

  signUpUser(formData: any) {
    // console.log(formData)
    if (formData !== null) {
      if (formData.password === formData.confirmPassword) {
        firebase.auth().createUserWithEmailAndPassword(formData.email, formData.password).then((userCredential) => {
          // console.log(userCredential)
          this.apiService.addUser(formData , userCredential.user.uid)
        }).catch((err) => {
          console.error(err)
        })
      } else {
        console.log('Password not match !!')
      }
    } else {
      console.log('Sorry, not have form data !!')
    }

  }

  signInUser(email: string, password: string) {
    return firebase.auth().signInWithEmailAndPassword(email, password).then((userCredential) => {
      console.log(userCredential)
    }).catch(err => {
      console.error(err)
    })
  }

  logout() {
    firebase.auth()
      .signOut()
      .then(res => {
        this.token = null
        this.authState = null
        this.user = null
        this.fetching['getUser'] ? this.fetching['getUser']() : ""
      })
      .catch(err => {
        console.log(err)
      });
  }

  getToken() {
    return this.token
  }

  isAuthenticated(): boolean {
    return this.authState !== null;
  }
}

import { Injectable } from '@angular/core';
import firebase from 'firebase/app';
import 'firebase/firestore'
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {


  constructor(private httpClient: HttpClient) { }

  addUser(data, uid) {
    data.uid = uid
    console.log(data)
    return this.httpClient.post(`${environment.apiURL}/addUser`, data).toPromise();
  }

  addData(data, collection) {
    const body = { ...data };
    body.collection = collection;
    body.uid = "test";
    return this.httpClient.post(`${environment.productsAPI}/addDataAutoId`, body).toPromise();
  }

  updateData(id, collection, data) {
    const body = { ...data };
    body.collection = collection;
    body.id = id;
    body.uid = "test";
    return this.httpClient.post(`${environment.productsAPI}/updateData`, body).toPromise();
  }

  daleteData(id, collection) {
    const body = {
      collection: collection,
      id: id,
      uid: "test"
    };
    return this.httpClient.post(`${environment.productsAPI}/deleteData`, body).toPromise();
  }

}

import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { TablesComponent } from '../../pages/tables/tables.component';
import { ProductStatusComponent } from '../../pages/product-status/product-status.component'
import { AddProductComponent } from '../../pages/add-product/add-product.component'
import { OrderHistoryComponent } from '../../pages/order-history/order-history.component'


export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'product-status', component: ProductStatusComponent },
    { path: 'add-product',    component: AddProductComponent },
    { path: 'order-history',  component: OrderHistoryComponent },
    { path: 'user-profile',   component: UserProfileComponent },
    // { path: 'tables',         component: TablesComponent },
    // { path: 'icons',          component: IconsComponent },
    // { path: 'maps',           component: MapsComponent }
];

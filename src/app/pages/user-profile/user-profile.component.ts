import { Component, OnInit } from '@angular/core';
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import { Router } from '@angular/router'


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  authState: any = null
  fetching: { [key: string]: any } = {}
  public user: { [key: string]: any } = {
    email: null,
  }

  constructor(public router: Router) {
    firebase.auth().onAuthStateChanged(res => {
      this.authState = res;
      if (this.authState) {
        if (this.fetching['getUser']) {
          this.fetching['getUser']()
        } else {
          this.fetching['getUser'] = firebase.firestore().collection('users').doc(this.authState.uid).onSnapshot(User => {
            if (User.data()) {
              this.user = User.data()
            }
          });
        }
      } else {
        this.toLogin()
      }

    }, err => {
      console.error(err)
    })
  }

  ngOnInit() {
  }

  toLogin() {
    this.router.navigate(['login'])
  }

}

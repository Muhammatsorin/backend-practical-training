import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { ApiService } from 'src/app/shared/apis/api.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit, OnDestroy {

  form: FormGroup;
  fetching: { [key: string]: any } = {}

  constructor(private formBuilder: FormBuilder , private apiService: ApiService) {
    this.form = this.formBuilder.group({
      productTitle: [null, Validators.required],
      purchasePrice: [null, Validators.required],
      retailsPrice: [null, Validators.required],
      brand: [null, Validators.required],
      manufacturer: [null, Validators.required],
      category: [null, Validators.required],
      quantity: [null, Validators.required],
      textArea: [null, Validators.required]
    })

  }

  ngOnInit(): void {
    this.onChangeFormbuilder()
  }

  onSubmit() {
    this.apiService.addData(this.form.value , "products").then((res) => {
      console.log(res)
    }).catch((err) => {
      console.log(err)
    })
  }

  onChangeFormbuilder() {
    this.fetching['unsubscribe productTitle'] = this.form.get('productTitle').valueChanges.subscribe(res => {
      console.log(res)
    })
    this.fetching['unsubscribe brand'] = this.form.get('brand').valueChanges.subscribe(res => {
      console.log(res)
    })
  }

  ngOnDestroy(): void {
    for (const key in this.fetching) {
      if (this.fetching.hasOwnProperty(key) && this.fetching[key]) {
        if (key.indexOf('unsubscribe') > -1) {
          this.fetching[key].unsubscribe();
        } else {
          this.fetching[key]();
        }
      }
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/shared/apis/api.service';
import firebase from 'firebase/app';
import 'firebase/firestore'


@Component({
  selector: 'app-product-status',
  templateUrl: './product-status.component.html',
  styleUrls: ['./product-status.component.scss']
})
export class ProductStatusComponent implements OnInit {

  dbProduct = firebase.firestore().collection("products")
  products: any

  constructor() { }

  ngOnInit(): void {
    this.dbProduct.get().then((querySnapshot) => {
      querySnapshot.forEach(doc => {
        this.products = doc.data()
        console.log(this.products)
      })
    }).catch((err) => {
      console.log(err)
    })
  }

}

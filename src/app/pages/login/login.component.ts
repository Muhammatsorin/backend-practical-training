import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { AuthService } from '../../shared/auth/auth.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  form: FormGroup;
  loading: boolean;
  fetching: { [key: string]: any } = {};

  constructor(private formBuilder: FormBuilder, private authService: AuthService) {
    this.loading = this.authService.loading;
    this.form = this.formBuilder.group({
      email: [null, [
        Validators.required,
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]
      ],
      password: [null, Validators.required]
    })
  }

  ngOnInit() {
  }

  onSubmit() {
    this.loading = true;
    this.authService.signInUser(this.form.get('email').value, this.form.get('password').value).then(res => {
      this.loading = false;
    })
  }

  ngOnDestroy(): void {
    for (const key in this.fetching) {
      if (this.fetching.hasOwnProperty(key) && this.fetching[key]) {
        if (key.indexOf('unsubscribe') > -1) {
          this.fetching[key].unsubscribe();
        } else {
          this.fetching[key]();
        }
      }
    }
  }

}

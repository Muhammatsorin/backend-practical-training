import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { AuthService } from '../../shared/auth/auth.service'


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  form: FormGroup;
  loading: boolean;

  constructor(private formBuilder: FormBuilder , private authService: AuthService) { 
    this.loading = this.authService.loading
    this.form = this.formBuilder.group({
      email: [null, [
        Validators.required,
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]
      ],
      password: [null, Validators.required],
      confirmPassword: [null , Validators.required],
      firstName: [null , Validators.required],
      lastName: [null , Validators.required],
      address: [null , Validators.required]
    })
  }

  ngOnInit() {
  }

  onCreate() {
    this.loading = true;
    if (this.form.get('email').value) {
      this.authService.signUpUser(this.form.value)
    } else {
      alert('Password not match !! Please enter your password again ..')
    } 
  }

}
